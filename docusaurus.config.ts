import { themes as prismThemes } from "prism-react-renderer";
import type { Config } from "@docusaurus/types";
import type * as Preset from "@docusaurus/preset-classic";

const config: Config = {
  title: "D7Security",
  tagline:
    "The D7Security group provides unofficial extended support for Drupal 7. We are working on Drupal 7 Long Term Support (LTS) to continue security and maintenance updates for selected contributed projects now that Drupal 7 has reached end of life (EOL) on Drupal.org.",
  favicon: "img/favicon.ico",

  // Set the production url of your site here
  url: "https://www.d7security.org/",
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: "/",

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: "D7Security", // Usually your GitHub org/user name.
  projectName: "D7Security website", // Usually your repo name.

  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
  },

  presets: [
    [
      "classic",
      {
        docs: {
          sidebarPath: "./sidebars.ts",
        },
        blog: {
          showReadingTime: true,
          blogSidebarCount: 'ALL',
        },
        theme: {
          customCss: "./src/css/custom.css",
        },
      } satisfies Preset.Options,
    ],
  ],

  // Add security-advisories as a second blog.
  plugins: [
    [
      '@docusaurus/plugin-content-blog',
      {
        id: 'security-advisories',
        routeBasePath: 'security-advisories',
        path: './security-advisories',
      },
    ],
  ],

  themeConfig: {
    // Replace with your project's social card
    image: "img/D7Security-social.png",
    navbar: {
      title: "D7Security",
      logo: {
        alt: "D7security Logo",
        src: "img/D7 security logo.svg",
      },
      items: [
        { to: "/docs/intro", label: "Info", position: "left" },
        { href: "/blog", label: "Blog", position: "left" },
        { href: "/security-advisories", label: "Security Advisories", position: "left" },
        { href: "https://gitlab.com/d7security/d7security/-/issues", label: "Issues", position: "left" },
        {
          href: "https://gitlab.com/d7security",
          label: "Gitlab",
          position: "left",
        },
      ],
    },
    colorMode: {
      defaultMode: "light",
      disableSwitch: true,
      respectPrefersColorScheme: false,
    },
    prism: {
      theme: prismThemes.github,
      darkTheme: prismThemes.dracula,
    },
  } satisfies Preset.ThemeConfig,
};

export default config;
