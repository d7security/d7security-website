---
sidebar_position: 2
date: 01-16-2025
---

# Webform Multiple File Upload - Critical - Cross Site Scripting - D7SECURITY-SA-CONTRIB-2025-001

**2025/01/16**

**Project:** [Webform Multiple File Upload](https://gitlab.com/d7security/webform_multifile)

**Security risk:** Critical [16/25 AC:Basic/A:None/CI:Some/II:Some/E:Theoretical/TD:All](https://www.drupal.org/security-team/risk-levels)

**Vulnerability:** Cross Site Scripting

**Affected versions:** `<7.x-1.7`

## Description

The Webform Multiple File Upload module allows users to upload multiple files on a Webform.

The module doesn't sufficiently escape filenames when displaying them, thereby exposing an XSS vulnerability.

This vulnerability is mitigated by the fact that an attacker must have access to a Webform that allows multiple file uploads.

## Solution

Install the latest version.

If you use the Webform Multiple File Upload module for Drupal 7, upgrade to [Webform Multiple File Upload 7.x-1.7](https://gitlab.com/d7security/webform_multifile/-/releases/7.x-1.7).

### Reported by
<ul>
<li><a href="https://www.drupal.org/user/102818">Michael Hess</a></li>
</ul>

### Fixed by
<ul>
<li><a href="https://www.drupal.org/user/36762">Greg Knaddison</a></li>
<li><a href="https://www.drupal.org/user/2609534">Rotem Reiss</a></li>
<li><a href="https://www.drupal.org/user/1945158">Tatiana Kiseleva</a></li>
<li><a href="https://www.drupal.org/user/1945174">Dmitry Kiselev</a></li>
<li><a href="https://www.drupal.org/user/796188">MustangGB</a></li>
<li><a href="https://www.drupal.org/user/100267">Moisés Rodríguez Carmona</a></li>
<li><a href="https://www.drupal.org/user/112790">Tom Keitel</a></li>
</ul>

### Coordinated by
<ul>
<li><a href="https://www.drupal.org/user/796188">MustangGB</a></li>
</ul>
