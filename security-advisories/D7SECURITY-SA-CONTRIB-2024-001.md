---
sidebar_position: 1
date: 02-28-2024
---

# Coffee - Moderately critical - Cross Site Scripting - D7SECURITY-SA-CONTRIB-2024-001

**2024/02/28**

**Project:** [Coffee](https://gitlab.com/d7security/coffee)

**Security risk:** Moderately critical [13/25 AC:Basic/A:Admin/CI:Some/II:Some/E:Theoretical/TD:All](https://www.drupal.org/security-team/risk-levels)

**Vulnerability:** Cross Site Scripting

**Affected versions:** `<7.x-2.4`

## Description

The Coffee module helps you to navigate through the Drupal admin menus faster with a shortcut popup.

The module doesn't sufficiently escape menu names when displaying them in the popup, thereby exposing an XSS vulnerability.

This vulnerability is mitigated by the fact that an attacker must have a role with the permission "Administer menus and menu links".

See also <a href="https://www.drupal.org/sa-contrib-2024-011">Coffee - Moderately critical - Cross Site Scripting - SA-CONTRIB-2024-011</a>.

## Solution

Install the latest version.

If you use the Coffee module for Drupal 7, upgrade to [Coffee 7.x-2.4](https://gitlab.com/d7security/coffee/-/releases/7.x-2.4).

### Reported by
<ul>
<li><a href="https://www.drupal.org/user/998680">Patrick Fey</a></li>
</ul>

### Fixed by
<ul>
<li><a href="https://www.drupal.org/user/919186">Michael Mol</a></li>
<li><a href="https://www.drupal.org/user/262198">Klaus Purer</a></li>
<li><a href="https://www.drupal.org/user/1781874">Oliver Köhler</a></li>
</ul>

### Coordinated by
<ul>
<li><a href="https://www.drupal.org/user/36762">Greg Knaddison</a> of the Drupal Security Team</li>
</ul>
