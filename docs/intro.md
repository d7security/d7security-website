---
sidebar_position: 1
---

# About D7 Security Group

## D7Security - Drupal 7 Long Term Support (LTS)

The D7Security group provides unofficial extended support for Drupal 7. We are working on Drupal 7 Long Term Support (LTS) to continue security and maintenance updates for selected contributed projects now that Drupal 7 has reached end of life (EOL) on Drupal.org.

Please check [General Information](/docs/category/general-information) and [Policies and Releases](/docs/category/policies-and-releases) for more information.

🚨 All security reports against Drupal 7 should still be reported via [security.drupal.org](https://security.drupal.org/node/add/project-issue/drupal) first! 🚨
