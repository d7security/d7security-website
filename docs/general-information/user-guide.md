---
sidebar_position: 2
---

# User Guide

**This page describes how you as a Drupal 7 site administrator can get update notifications from the D7Security group.**

## Install the d7security_client module
Follow the [Drupal 7 guide how to install modules](https://www.drupal.org/docs/7/extend/installing-modules), with the difference that you download the tar.gz file from the [latest d7security_client release page on Gitlab](https://gitlab.com/d7security/d7security_client/-/releases/permalink/latest).
Example for downloading and enabling the module with drush:

```
cd /path/to/drupal-7/sites/all/modules/contrib
curl https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.3/d7security_client-7.x-1.3.tar.gz | tar xz
drush en d7security_client
```

## Check for updates
In the admin backend of your Drupal 7 site go to `/admin/reports/updates`. Click the link "Check manually". Any projects marked red as "security update" or "unsupported" indicate that you have an outdated version of a module enabled.

![check-updates](./img/check-updates.png)

### Unsupported projects
Unsupported projects mean that neither drupal.org nor the D7Security group are providing support for the project right now. Your options:

1. You can either disable and uninstall the module if you don't need it anymore
2. Or you can [open an issue in the D7Security project](https://gitlab.com/d7security/d7security/-/issues/new) and ask to get it supported again.


### Security updates
Security updates include a link to the release page on either drupal.org or the new project in D7Security if it is supported here. Download and install the security release as you normally would to update the module. The only difference for D7Security supported projects is that you download the tarball from our Gitlab.com release pages instead of drupal.org.

## Enable update notifications
To benefit from security releases we make in the D7security group it is important that you enable update notifications per email for your site. In the admin backend of your Drupal 7 site go to /admin/reports/updates/settings, provide an email address and make sure at least notifications for security updates are enabled.

![check-updates](./img/update-settings.png)

## Ensure your contrib modules have "version" and "project" in info files
The Drupal 7 update notification system can only work if the contrib modules installed on your site have version and project set in their info files.

Do not use git checkouts of contrib modules on production sites, as they usually don't have version information.
Do not download the original "source" archives of D7Security Gitlab projects, as they did not go through the packaging process. Use the properly packaged `<module_name>-<version>.tar.gz` download links on the Gitlab release pages.

![check-updates](./img/release-downloads.png)
