---
sidebar_position: 6
---

# Options for Drupal 7 site owners

- Migrate to Drupal 11
- Migrate to Backdrop CMS
- Migrate to another system (Wordpress etc)
- Stay on Drupal 7 and buy a [D7 Extended Security Support contract](https://www.drupal.org/about/drupal-7/d7eol/migration-resource-center/d7-extended-security-support)
- Stay on Drupal 7, patch security problems yourself (and share fixes with D7Security)

The [Drupal 7 Soft Landing Initiative](https://www.drupal.org/community-initiatives/drupal-7-soft-landing-initiative) provides good guidance on this!
