---
sidebar_position: 4
---

# Mission, Values and Philosophy

The mission of the D7Security group is to provide extended support for selected Drupal 7 modules and themes. This is an unofficial effort independent from drupal.org. We are working on Drupal 7 Long Term Support (LTS) to continue security and maintenance updates for selected Drupal 7 contributed projects now that Drupal 7 has reached end of life (EOL) on Drupal.org.

## Code of Conduct
All participants in D7Security-related spaces must follow the [Drupal Code of Conduct](https://www.drupal.org/dcoc). Please report Code of Conduct violations per email to klaus.purer [at] protonmail.ch for now (a conflict resolution group will be established later).

## Shared Values and Philosophy
All members of the D7Secrutiy group agree to these guiding principles and values:
- There will be no branding of D7Security as a Drupal Classic fork.
- We accept that Drupal 7 is legacy software. Developers should not use Drupal 7 to launch new sites, they should use Drupal 10 or Backdrop instead. We don't argue about which CMS system is better in the D7Security space.
- The mission of D7security is to ensure stability, security and PHP compatibility of Drupal 7 sites, so that they have more time to upgrade and migrate. We are not interested in developing new features or doing code refactoring in Drupal 7 modules and want to keep changes to a minimum.
- We respect the Drupal Security Team and the Drupal Association and their decisions of not supporting Drupal 7 in the future. We want to make all our lives easier and make this initiative a win-win for Drupal 10 maintainers/release managers and Drupal 7 maintainers.
- We accept that security fixes will be released for Drupal 10 modules first and Drupal 7 second. This is important so that Drupal 10 maintainers and Security team are not delayed on coordinating with Drupal 7 module releases. Yes, running legacy software like Drupal 7 implies a higher security risk, but we believe that we can mitigate it well enough with this D7Security initiative.
