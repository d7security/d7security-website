---
sidebar_position: 1
---

# How D7Security Works

## Background
The Drupal Security Team has announced in [PSA-2023-06-07](https://www.drupal.org/psa-2023-06-07) that unsupported Drupal 7 modules/themes cannot be supported again on drupal.org. That is a problem for Drupal 7 maintainers and contributors, as they don't have a shared space where they can collaborate and publish security fixes for those Drupal 7 projects.
After a [discussion on drupal.org](https://www.drupal.org/project/ideas/issues/3408125) a group of contributors decided to create the unofficial D7Security group on Gitlab.com, where they can publish new releases for unsupported drupal.org projects. D7Security is an open source project where members from multiple companies collaborate on keeping Drupal 7 projects supported.
By installing the d7security_client helper module Drupal 7 site administrators can receive update notifications about new releases available from D7Security. Check the [User guide](/docs/general-information/user-guide) how to participate!
The supported Drupal 7 projects are forked from their drupal.org Git repositories into Gitlab.com repositories, see for example the forked [devel](https://gitlab.com/d7security/devel) or [message](https://gitlab.com/d7security/message) module. Releases are created by D7Security members via a semi-automated Gitlab workflow, which publishes into the provided project update XML to notify site administrators.

## Technical workflow
To distribute update notifications, the [d7security_client module](https://gitlab.com/d7security/d7security_client/-/releases/permalink/latest) adds an additional source for supported projects. It fetches a list of D7Security supported projects [see supported_projects.txt](https://gitlab.com/d7security/d7security/-/blob/main/supported_projects.txt) and overrides the update XML source to check. The Drupal 7 site will contact now both drupal.org (for projects that are still supported there) and D7Security's gitlab.com for available updates.

![d7security_flow](./img/d7security_flow.svg)
