---
sidebar_position: 3
---

# D7Security update status module

[d7security_client](https://gitlab.com/d7security/d7security_client/-/releases/permalink/latest) is the name of the update status module from the D7Security group. Please install it on your Drupal 7 sites by following our [user guide](/docs/general-information/user-guide).
