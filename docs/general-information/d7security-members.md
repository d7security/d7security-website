---
sidebar_position: 5
---

# D7Security members and joining

## Current security release members
These members are administrators of the D7Security group on Gitlab.com. They control the list of supported projects and grant permissions to new members.

- Klaus Purer ([klausi on drupal.org](https://www.drupal.org/u/klausi), [klausi1 on gitlab.com](https://gitlab.com/klausi1))
- Ivan Tibezh ([tibezh on drupal.org](https://www.drupal.org/u/tibezh), [tibezh on gitlab.com](https://gitlab.com/tibezh))
- Juraj Falat ([durifal on drupal.org](https://www.drupal.org/u/durifal), [durifal on gitlab.com](https://gitlab.com/durifal))
- Andrii Cheredn ([andriic on drupal.org](https://www.drupal.org/u/durifal), [onesixromcom on gitlab.com](https://www.drupal.org/u/durifal))
- John Locke ([freelock on drupal.org](https://www.drupal.org/u/freelock), [freelock_john on gitlab.com](https://gitlab.com/freelock_john))
- Jen Lampton ([jenlampton on drupal.org](https://www.drupal.org/u/jenlampton), [jenlampton on gitlab.com](https://www.drupal.org/u/jenlampton))
- Gregor Sýkora ([gresko8 on drupal.org](https://www.drupal.org/u/gresko8), [gresko2 on gitlab.com](https://gitlab.com/gresko2))
- Allison Vorthmann ([allisonherodevs on drupal.org](https://www.drupal.org/u/allisonherodevs), [aavorthmann on gitlab.com](https://gitlab.com/aavorthmann))
- Caroline Boyden ([cboyden on drupal.org](https://www.drupal.org/u/cboyden), [cboyden on gitlab.com](https://gitlab.com/cboyden))
- Yogesh Pawar ([yogeshmpawar on drupal.org](https://www.drupal.org/u/yogeshmpawar), [yogeshmpawar on gitlab.com](https://gitlab.com/yogeshmpawar))
- Sinduri Guntupalli ([sinduri on drupal.org](https://www.drupal.org/u/sinduri), [sinduri.g on gitlab.com](https://gitlab.com/sinduri.g))
- Alex Burrows ([aburrows on drupal.org](https://www.drupal.org/u/aburrows), [abdesignuk on gitlab.com](https://gitlab.com/abdesignuk))
- Paul McKibben ([paulmckibben on drupal.org](https://www.drupal.org/u/paulmckibben), [paulmckibben on gitlab.com](https://gitlab.com/paulmckibben))
- Ra Mänd ([ram4nd on drupal.org](https://www.drupal.org/u/ram4nd), [ram4nd on gitlab.com](https://gitlab.com/ram4nd))
- Alex Pott ([alexpott on drupal.org](https://www.drupal.org/u/alexpott), [alexpott on gitlab.com](https://gitlab.com/alexpott))
- Vijay Mani ([vijaycs85 on drupal.org](https://www.drupal.org/u/vijaycs85), [vijaycs85 on gitlab.com](https://gitlab.com/vijaycs85))
- Henri Médot ([anrikun on drupal.org](https://www.drupal.org/u/anrikun), [anrikun on gitlab.com](https://gitlab.com/anrikun))
- MustangGB ([mustanggb on drupal.org](https://www.drupal.org/u/mustanggb), [mustanggb on gitlab.com](https://gitlab.com/mustanggb))
- Roderik Muit ([roderik on drupal.org](https://www.drupal.org/u/roderik), [rmuit on gitlab.com](https://gitlab.com/rmuit))

## Current general project members
These members are project maintainers of individual projects.

- Ayush Mishra ([ayushmishra206 on drupal.org](https://www.drupal.org/u/ayushmishra206), [ayushmishra206 on gitlab.com](https://gitlab.com/ayushmishra206))


## Joining the D7Security group
We are looking for all kinds of people in different roles working with Drupal 7 projects. We need developers, communicators, project managers, designers, marketers - there are a lot of tasks in an Open Source project like this.

Please reach out to Klausi per email (klaus.purer [at] protonmail.ch) with an introduction of yourself and how you would like to help.

Of course it is fine to join discussions as a non-member as well!

Prerequisites for developers that are allowed to make security releases in this group:
- Agreeing with the [Values](/docs/general-information/mission-values-philosophy) of the D7Security initiative.
- Experienced in Drupal 7 module or theme development
- Good understanding of Drupal 7 [Writing secure code](https://www.drupal.org/docs/7/security/writing-secure-code)
- Understanding of the [Drupal Security Team process](https://www.drupal.org/docs/develop/issues/issue-procedures-and-etiquette/reporting-a-security-issue) and coordinated vulnerability disclosure.
