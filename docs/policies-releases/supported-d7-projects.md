---
sidebar_position: 2
---

# Supported Drupal 7 projects

The current list of D7Security supported projects can be found in the machine-readable list [supported_modules.txt](https://gitlab.com/d7security/d7security/-/blob/main/supported_projects.txt).

The D7Security group will only support Drupal 7 projects if there is at least one member of the D7Security group willing to maintain and support it.

Examples:

- [The Devel module](https://www.drupal.org/project/devel) is supported by D7Security and the code was forked to [https://gitlab.com/d7security/devel](https://gitlab.com/d7security/devel)

**[Supported Drupal 7 projects](https://gitlab.com/d7security/d7security/-/wikis/Supported-Drupal-7-projects#maintainers-for-projects)**
