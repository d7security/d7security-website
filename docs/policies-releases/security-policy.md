---
sidebar_position: 1
---

# Security policy

This page describes how security vulnerability reports and releases are handled.

1. 🚨 All security reports against Drupal 7 core and contrib projects must still be reported via security.drupal.org first!
2. If a security issue is reported by accident to the D7Security group through our Communication channels, then the report will be forwarded to the Drupal Security team and all public information about it will be deleted or made private.
3. If a D7Security group member has access to an ongoing private discussion on security.drupal.org then they must not share private details with the rest of the D7Security group. Once the details are released or allowed to be made public by the Drupal Security team then the D7Security group can follow up with discussions and releases.
