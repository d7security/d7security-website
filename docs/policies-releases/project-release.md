---
sidebar_position: 3
---

# Making a project release

This document describes how the D7Security group members make a new Drupal 7 project release in the D7Security group.

## Forking the drupal.org repository

Ensure that the project repository exists in D7security. If it does not:

1. Create new empty Gitlab repository at [https://gitlab.com/projects/new#blank_project](https://gitlab.com/projects/new#blank_project)
    - Project name: machine name of the drupal.org project. Example: "devel"
    - Project URL: d7security /  . Example: "d7security / devel"
    - deployment target: empty
    - Visibility level: public
    - Project configuration: no options set

2. Git clone project from drupal.org and checkout Drupal 7 branch that should be supported by D7Security. Example:

```git clone https://git.drupalcode.org/project/devel.git
cd devel
git checkout 7.x-1.x
```

3. Add Gitlab origin as default and push branch. Example for devel:

```
git remote rename origin old-origin
git remote add origin git@gitlab.com:d7security/devel.git
git push origin 7.x-1.x
```

4. Push all 7.x tags

```
git push origin `git tag | grep 7.x-`
```

5. Add Gitlab CI config that makes release pages and tarballs with info file timestamps:

```
printf "include:\n  - component: gitlab.com/d7security/d7security_gitlab_components/d7_gitlab_release@main" > .gitlab-ci.yml
git add .gitlab-ci.yml
git commit -m "chore(gitlab): Add Gitlab config to create releases from git tags"
git push origin
```

6. Remove the old drupal.org git remote:

```
git remote remove old-origin
```

## Making changes
Before making a new release ensure that all required bugfixes (and potential security fixes) are applied to the 7.x-X.x branch in the repository correctly.

## Tagging and pushing a new release tag
This will automatically trigger a Gitlab pipeline and create a release page on Gitlab with tarballs. Example:

```
git tag 7.x-1.8
git push origin 7.x-1.8
```

## Editing release notes
Wait until the Gitlab pipeline has created a release page from the git tag. Then you can edit the release page and add details about the release, like a changelog summary. There is an edit button/link, example: [https://gitlab.com/d7security/d7security_test/-/releases/7.x-1.5/edit](https://gitlab.com/d7security/d7security_test/-/releases/7.x-1.5/edit)

## Optional: Marking the release as security update
If (and only if!) the release should be marked as security update then put the words "Security update" manually in the description field of the release page.
The release notes description will later be read by the update XML script to decide if the release should be marked as security update or not.

## Add project to supported_projects.txt
Ensure that the project is listed in [supported_projects.txt](https://gitlab.com/d7security/d7security/-/blob/main/supported_projects.txt). If it is not there yet create a merge request against the d7security repository and seek approval from another D7Security group member.

## Update XML changes
There is a Gitlab CI job to publish any new releases in the update XML. [Trigger the job manually in the Gitlab web UI](https://gitlab.com/d7security/d7security/-/pipelines/new). You can leave the variables PROJECT_NAME and PROJECT_VERSION empty, the list of new releases will be retrieved automatically. Click "Run pipeline".
