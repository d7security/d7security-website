import clsx from "clsx";
import styles from "./styles.module.css";
import { slackSVG, gitlabSVG, mailSVG, circleExclamationSVG } from "@site/src/svg";
import { externalLinkSVG } from "@site/src/svg";

export const CommunicationChannels = (): JSX.Element => (
    <section
        className={clsx("container--fluid", styles.communicationChannels)}
    >
        <article className="centered">

            <h2>Communication Channels</h2>
            <div
                className={clsx(
                    styles.communicationChannelsCardGroup
                )}
            >
                <div className={clsx(styles.communicationChannelCard)}>
                    <a href="https://app.slack.com/client/T06GX3JTS/C06AP6GE05P" target="_blank" rel="noreferrer">
                        {slackSVG}
                        <p>There is a #d7security channel in Drupal Slack.</p>
                    </a>
                </div>

                <div className={clsx(styles.communicationChannelCard)}>
                    <a href="https://gitlab.com/d7security/d7security/-/issues" target="_blank" rel="noreferrer">

                        {gitlabSVG}
                        <p>
                            The main communication channel for coordination are Gitlab
                            issues in the D7Security project
                        </p>
                    </a>
                </div>
                <div className={clsx(styles.communicationChannelCard)}>
                    <a href="https://gitlab.com/d7security/d7security/-/issues/2" target="_blank" rel="noreferrer">

                        {mailSVG}
                        <p>
                            Subscribe to notifications on the project announcement Gitlab
                            issue, which serves as a newsletter.
                        </p>
                    </a>
                </div>
            </div>
            <div className="alert-custom alert-primary mt-16" role="alert">
                {circleExclamationSVG}
                <span>All security reports against Drupal 7 core and contribution
                    projects must still be reported via <a href="https://security.drupal.org" target="_blank" rel="noreferrer" aria-label="Drupal Security Website">security.drupal.org {externalLinkSVG} </a> first!
                </span>
            </div>
        </article>
    </section>
)
