interface BlogPost {
    title: string;
    body: string;
    link: string;
}
export const blogs: BlogPost[] = [
    {
        title: 'The D7Security Newsletter Jan 28,2025',
        body: 'Website launch, project usage telemetry, new supported projects, new members',
        link: '/blog/01-28-2025-d7security-team-newsletter'
    },
    {
        title: 'The D7Security Newsletter Oct 9,2024',
        body: 'Website launch, project usage telemetry, new supported projects, new members',
        link: '/blog/10-09-2024-d7security-team-newsletter'
    },
    {
        title: 'What happens when a Drupal 7 module used by 70,000 sites gets unsupported?',
        body: `The story of how Drupal 7 maintenance and security support is continued.`,
        link: '/blog/08-23-2024-what-happens-when-a-drupal-7-module-used-by-70000-sites-gets-unsupported'
    },
]
