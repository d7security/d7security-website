import clsx from "clsx";

import { blogs } from './blogs';
import styles from "./styles.module.css";
import { chevronRightSVG } from "@site/src/svg";

export const Blog = ({ title, body, link }) => (
    <div className={clsx(styles.blogPost)}>
        <div className={clsx(styles.blogPostImagePlaceholder)}></div>
        <a className={clsx(styles.blogPostDescription)} href={link} rel="noreferrer">
            <span className={clsx(styles.blogPostDescriptionTitle)}>
                {title}
            </span>
            <p className={clsx(styles.blogPostDescriptionText)}>
                {body}
            </p>
            <p>Read More {chevronRightSVG}</p>
        </a>
    </div>
)

export const HomepageBlog = () => (
    <section className={clsx("container--fluid", styles.blog)}>
        <article className="centered">
            <h2>Blog posts and Newsletters</h2>
            <div
                className={clsx(
                    styles.blogPostGroup
                )}
            >
                {blogs.map(({ title, body, link }) => (<Blog key={link} title={title} body={body} link={link} />))}
            </div>
        </article>
    </section>
)
