import clsx from "clsx";
import styles from "./styles.module.css";
import { externalLinkSVG } from "@site/src/svg";

export const HomepageFooter = (): JSX.Element => (
  <footer className={clsx("container--fluid", styles.footer)}>
    <div className={clsx(styles.footerLinkGroups)}>
      <div className={clsx(styles.footerLinkGroup)}>
        <span className={clsx(styles.footerLinkGroupHeader)}>
          Policies and Releases
        </span>
        <a
          className={clsx(styles.footerLink)}
          href="/docs/policies-releases/security-policy"
        >
          Security policy
        </a>
        <a
          className={clsx(styles.footerLink)}
          href="/docs/policies-releases/supported-d7-projects"
        >
          Supported Drupal 7 projects
        </a>
        <a
          className={clsx(styles.footerLink)}
          href="/docs/policies-releases/project-release"
        >
          Making a project release
        </a>
      </div>
      <div className={clsx(styles.footerLinkGroup)}>
        <span className={clsx(styles.footerLinkGroupHeader)}>Docs</span>
        <a
          className={clsx(styles.footerLink)}
          href="/docs/general-information/how-d7-security-works"
        >
          How D7 Security Works
        </a>
        <a
          className={clsx(styles.footerLink)}
          href="/docs/general-information/user-guide"
        >
          User Guide
        </a>
        <a
          className={clsx(styles.footerLink)}
          href="/docs/general-information/d7security-update-status-module"
        >
          D7Security update status module
        </a>
      </div>
      <div className={clsx(styles.footerLinkGroup)}>
        <span className={clsx(styles.footerLinkGroupHeader)}>Members</span>
        <a
          className={clsx(styles.footerLink)}
          href="/docs/general-information/d7security-members"
        >
          D7Security Members & Joining
        </a>
        <a className={clsx(styles.footerLink)} href="/docs/communication">
          Communication Channels
        </a>
      </div>
      <div className={clsx(styles.footerLinkGroup)}>
        <span className={clsx(styles.footerLinkGroupHeader)}>Security</span>
        <a
          className={clsx(styles.footerLink)}
          href="/docs/general-information/mission-values-philosophy"
        >
          Mission, Values and Philosophy
        </a>
        <a className={clsx(styles.footerLink)} href="/security-advisories">
          Security Advisories
        </a>
      </div>
    </div>
    <p className="copyright">
      © {new Date().getFullYear()} D7Security | All Rights Reserved
      <br />
      <span>
        Drupal is a{" "}
        <a
          className={clsx(styles.footerLink)}
          href="https://www.drupal.org/about/trademark"
          target="_blank"
          rel="noreferrer"
        >
          registered trademark {externalLinkSVG}
        </a>{" "}
        of{" "}
        <a className={clsx(styles.footerLink)} href="https://dri.es/" target="_blank" rel="noreferrer">
          Dries Buytaert {externalLinkSVG}
        </a>
        .
      </span>
    </p>
  </footer>
);
