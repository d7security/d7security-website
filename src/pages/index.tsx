import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Heading from "@theme/Heading";
import Layout from "@theme/Layout";
import clsx from "clsx";
import { externalLinkSVG } from "../svg";

import { HomepageBlog } from "../components/homepage/blog/blog";
import { CommunicationChannels } from "../components/homepage/communication-channels/communical-channels";
import { HomepageFooter } from "../components/homepage/footer/footer";
import styles from "./index.module.css";

function HomepageHeader(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (
    <header className={clsx("hero hero--primary", styles.heroBanner)}>
      <div className="container">
        <Heading as="h1" className="hero__title">
          {siteConfig.title}
        </Heading>
        <p className="hero__subtitle col col--6 col--offset-3">
          {siteConfig.tagline}
        </p>
        <div className={styles.buttons}>
          <Link
            className="button-custom filled-button"
            to="/docs/category/general-information"
          >
            General Information
          </Link>
          <Link
            className="button-custom outline-button"
            to="/docs/category/policies-and-releases"
          >
            Policies and Releases
          </Link>
        </div>
      </div>
    </header>
  );
}

export default function Home(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title="Extended support for Drupal 7"
      description="The D7Security provides unofficial extended support to Drupal 7. We are working on LTS to continue security and maintenance updates for Core and contributed projects."
    >
      <HomepageHeader />
      <main>
        <section className={clsx("container--fluid", styles.lightBlock)}>
          <h2>Mission, Values and Philosophy</h2>
          <p>
            The mission of the D7Security group is to provide extended support
            for selected Drupal 7 modules and themes.
            <br />
            This is an unofficial effort independent from drupal.org
          </p>
          <a href="/docs/general-information/mission-values-philosophy">
            Read more about our values
          </a>
        </section>
        <section className={clsx("container--fluid", styles.whiteBlock)}>
          <section>
            <h2>How D7Security works</h2>
            <p>
              The Drupal Security Team has announced in{" "}
              <a
                href="https://www.drupal.org/psa-2023-06-07"
                target="_blank"
                rel="noreferrer"
                aria-label="Drupal PSA-2023-06-07"
              >
                PSA-2023-06-07 {externalLinkSVG}
              </a>{" "}
              that unsupported Drupal 7 modules/themes cannot be supported again
              on drupal.org. That is a problem for Drupal 7 maintainers and
              contributors, as they don't have a shared space where they can
              collaborate and publish security fixes for those Drupal 7
              projects.
            </p>
            <p>
              After a{" "}
              <a
                href="https://www.drupal.org/project/ideas/issues/3408125"
                target="_blank"
                rel="noreferrer"
                aria-label="Drupal.org Discussion on D7Security"
              >
                discussion on drupal.org {externalLinkSVG}
              </a>{" "}
              a group of contributors decided to create the unofficial
              D7Security group on Gitlab.com, where they can publish new
              releases for unsupported drupal.org projects. D7Security is an
              open source project where members from multiple companies
              collaborate on keeping Drupal 7 projects supported.
            </p>
            <p>
              To participate as Drupal 7 site owner follow our <a href="/docs/general-information/user-guide">user guide</a> and install the{" "}
              <a
                href="https://gitlab.com/d7security/d7security_client/-/releases/permalink/latest"
                target="_blank"
                rel="noreferrer"
                aria-label="d7security_client release page"
              >
                d7security_client module {externalLinkSVG}
              </a>{" "}.
            </p>
            <div className={styles.buttonsLeft}>
              <a
                className="button-custom outline-button-primary"
                href="/docs/category/general-information"
              >
                Learn More
              </a>
              &nbsp;
              <a
                className="button-custom outline-button-primary"
                href="/docs/general-information/how-d7-security-works"
              >
                Technical Workflow
              </a>
            </div>
          </section>
          <div className={clsx(styles.imgPlaceholder)}></div>
        </section>
        <section className={clsx("container--fluid", styles.lightBlock)}>
          <h2 className={clsx(styles.textCenter)}>
            Drupal 7 has reached End of Life
          </h2>
          <p>
            There are various options for D7 site owners. The Drupal 7 Soft
            Landing Initiative provides good guidance on this!
          </p>
          <div className={clsx(styles.buttons)}>
            <a
              type="button"
              href="/docs/general-information/options-d7-owners"
              className="button-custom outline-button-primary"
            >
              Suggested Options
            </a>
            <a
              type="button"
              href="https://www.drupal.org/community-initiatives/drupal-7-soft-landing-initiative"
              target="_blank"
              className="button-custom filled-button-primary"
            >
              Drupal 7 Soft Landing Initiative {externalLinkSVG}
            </a>
          </div>
        </section>
        <CommunicationChannels />
        <HomepageBlog />
        <HomepageFooter />
      </main>
    </Layout>
  );
}
