---
slug: 08-23-2024-what-happens-when-a-drupal-7-module-used-by-70000-sites-gets-unsupported
title: What happens when a Drupal 7 module used by 70,000 sites gets unsupported?
authors:
  name: Klaus Purer
  title: Principal Engineer at Jobiqo
  image_url: ./img/profile-images/klausi.jpeg
tags: [Drupal7, D7Security]
date: 08-23-2024
---

The official Drupal 7 end of life date is still a couple of months away. Even before the Drupal 7 extended support phase starts, Drupal 7 site owners are already facing the problem of Drupal 7 contributed modules becoming unsupported. The maintainers of such Drupal 7 modules decide that they don't want to support them anymore and make a fatal mistake: they mark the module as unsupported on drupal.org. Per drupal.org's policy [PSA-2023-06-07](https://www.drupal.org/psa-2023-06-07) security support cannot be enabled again for the module, so even if there are new maintainers willing to take over they can't do that on drupal.org.

### Views Slideshow: 70,000 site installations

This happened recently to the [Views Slideshow module](https://www.drupal.org/project/views_slideshow) which is currently [used on 70,000 Drupal 7 sites](https://www.drupal.org/project/usage/views_slideshow). What the maintainers might not have realized: when they abandon security support for such a widely used module thousands of emails are sent out by Drupal's own update status module to site owners. A red light warning turns on in the admin backend showing that there is a potential security problem:

![Screenshot of the Drupal status report page with text: "The installed version of at least one of your modules or themes is no longer supported. Upgrading or disabling is strongly recommended. See the project homepage for more details. See the available updates page for more information and to install your missing updates."](img/status_unsupported.png)

And on the updates page:

![Screenshot of the Drupal Updates page: "Views Slideshow 7.x-3.10. Project not supported: This project is no longer supported, and is no longer available for download. Disabling everything included by this project is strongly recommended!"
](img/views_slideshow_unsupported.png)

I don't blame the maintainers: they don't run Drupal 7 sites anymore and overlooked the large user base. The problem is getting out of this process failure to put the Views Slideshow module back on security support.

The strength of the Drupal community is that people care and try to find solutions: [a8w4](https://www.drupal.org/u/a8w4) opened an [issue for discussion](https://www.drupal.org/project/views_slideshow/issues/3469409).

### D7Security takes over maintenance

As it happens my clients also use the Views Slideshow module. In the D7Security group we have a process in place to take over maintenance of abandoned modules. We did this before for other projects (see [Supported Projects](/docs/policies-releases/supported-d7-projects)) and also released [security advisories](/security-advisories). Both our policy conditions are met: D7Security members use the Views Slideshow module and it is unsupported on drupal.org.

Gregor and I worked on transferring the source code from drupal.org to our [new Views Slideshow repository](https://gitlab.com/d7security/views_slideshow). We made a [new release](https://gitlab.com/d7security/views_slideshow/-/releases/7.x-3.11) according to [our release process](/docs/policies-releases/project-release) to establish the new home of Views Slideshow. I also reached out to the former maintainers of the module if there are any known unresolved security vulnerabilities - we want to deal with them to protect our sites.

This is great so far but there is an important obstacle: only Drupal 7 sites that followed our [user guide](/docs/general-information/user-guide) will know that the Views Slideshow module is supported again. If you run Drupal 7 sites I urge you to [install the d7security_client module](/docs/general-information/user-guide) to join our shared Drupal 7 maintenance effort!

### A plea to Drupal 7 maintainers

**While Drupal 7 is still supported I plea to all Drupal 7 maintainers to not mark modules as unsupported on drupal.org. Please? Please! Please please please.**

Instead, you can reach out to us, the D7Security group via [our communication channels](/docs/communication). If we use the project we are happy to take over maintainership on drupal.org. Releases on drupal.org reach many more users than when they are forked here in D7Security.

### What about official commercial Drupal 7 support providers?

I'm very happy that there are now at least 2 (soon 3) [official commercial providers for Drupal 7 extended support](https://www.drupal.org/about/drupal-7/d7eol/migration-resource-center/d7-extended-security-support). They have only been announced recently and focus on supporting Drupal 7 from next year on. This is a bit disappointing to me, as there is already a security support need right now with the Views Slideshow module being only one example. I hope we can improve on that soon - I want to collaborate with the commercial providers as much as their business model allows.

It feels weird to write this: currently the best Drupal 7 security support is coming from community projects (the Drupal Security Team + the D7Security Group) because the commercial Drupal 7 extended support providers are not maintaining or releasing anything yet.

### Conclusion

There is a path forward for Drupal 7 contrib module support - we can catch unsupported modules and distribute update information with our D7Security project. Join us! 🤗
