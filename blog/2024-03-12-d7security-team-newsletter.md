---
slug: 03-12-2024-d7security-team-newsletter
title: The D7Security Newsletter March 12, 2024
authors:
  name: Klaus Purer
  title: Principal Engineer at Jobiqo
  image_url: ./img/profile-images/klausi.jpeg
tags: [Drupal7, D7Security, Newsletter]
date: 03-12-2024
---

Welcome to the third D7Security newsletter!

### First security release
---
On February 28th the D7Security group released their first security update of a contributed Drupal 7 module. In coordination with the Drupal Security Team new versions of the Coffee module were published on drupal.org for Drupal 10 and on gitlab.com for Drupal 7. The Coffee module is now in the list of supported D7Security projects.
This is a big milestone for the D7Security group and proves that our technical setup is capable of distributing and notifying Drupal 7 site owners of new Drupal 7 security updates. Special thanks to Greg Knaddison and Oliver Köhler for helping me with the Coffee release!
### New supported modules: coffee, ldap, simple_gmap
---
Besides the already mentioned Coffee module we took 2 more modules under the D7Security umbrella and released new versions for them: ldap and simple_gmap. This was only possible because of a new D7Security member that stepped up and contributed these 2 modules, which brings me to my next point ...
### New D7Security member: Caroline Boyden
---
I'm happy that Caroline joined and was able to release the ldap and simple_gmap module completely on her own just by following our release documentation. I'm relieved that other contributors find their way around how to get stuff done in the D7Security group, which also validates my ideas how to operate and that this project is not dependent on myself.
Thanks a lot Caroline!
### Planning security advisories
---
Jen Lampton brought up the topic of security advisories, which the D7Security group does not publish yet. I opened a discussion on D7Security advisories, planning something similar as Drupal.org is doing at https://www.drupal.org/security/ or Backdrop at https://backdropcms.org/security/advisories .
The main purpose of D7Security is to provide security alerts and updates, so I think advisory posts are a good idea as well.
### Improved Wiki documentation
---
I improved the start page and menu sidebar in our Wiki to make it more clear. I also added an overview page how D7Security works, check it out!
Presentation at Drupal Austria meetup
I will do a short presentation about D7Security at the Drupal Austria March Meetup. I will post the slides afterwards and maybe I can also find the time to do a video recording.
### d7security.org website
---
The D7Security website is still work in progress, we have some designs that we are currently reviewing. More updates will follow!
That's it for this newsletter, let me know if you have any questions!
