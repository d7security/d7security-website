---
slug: 12-24-2023-d7security-team-newsletter
title: The D7Security Newsletter December 24, 2023
authors:
  name: Klaus Purer
  title: Principal Engineer at Jobiqo
  image_url: ./img/profile-images/klausi.jpeg
tags: [Drupal7, D7Security, Newsletter]
date: 12-24-2023
---

Welcome to the first D7Security newsletter! The D7Security initiative is now starting up and we have accomplished a couple of things so far:

### Gitlab wiki documentation
---
There is a lot of written documentation in the Gitlab wiki already. I would like to highlight the mission and values page here that outlines the D7Security philosophy.
### User Guide
---
The first version of a user guide is now available in the wiki. It describes how Drupal 7 site administrators can get updates of supported modules by the D7Security initiative.
### Website and logo
---
A dummy page is now live at https://www.d7security.org/ and we have a logo for the D7Security group! Special thanks to Sebastian Gilits for generating the logo.
I'm looking for help to make the website pretty, accessible and informative. Please get in touch at the Gitlab issue if you would like to contribute!
### Technical prototype validated
---
With the fork of the Drupal 7 Devel module we have validated in a prototype that the release process and update status functionality works on Gitlab. The release process was also documented in the wiki.
We are ready to release more Drupal 7 modules in the D7Security group!
### New D7Security members
---
I'm happy to announce that I could add 3 senior developers as members to the D7Security group. Thanks a lot Ivan, Juraj and Andrii for joining!
I'm in contact with more developers as well, please check the wiki page if you are interested in joining!
### Next steps
---
Besides building out the d7security.org website there are a couple of small TODOs being collected.
The next bigger milestone will be the first security release of a module in the D7security group, once we encounter such a case.
Thank you all for your input and support!
