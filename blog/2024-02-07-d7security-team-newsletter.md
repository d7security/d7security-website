---
slug: 02-07-2024-d7security-team-newsletter
title: The D7Security Newsletter February 7, 2024
authors:
  name: Klaus Purer
  title: Principal Engineer at Jobiqo
  image_url: ./img/profile-images/klausi.jpeg
tags: [Drupal7, D7Security, Newsletter]
date: 02-07-2024
---

Welcome to the second D7Security newsletter!

### Podcast episode about D7security
---
I did an episode on the Drupal 7 End-of-Life podcast with Mark and Chris from Chromatic. It was a pleasure to talk with them and you can get a lot of background information on the D7Security group motivation.
### New D7Security members
---
I'm very happy to announce that several more people have stepped up as members of the D7Security group:

- **John Locke**
- **Jen Lampton** Backdrop CMS Security team member who can can help us coordinate with them
- **Gregor Sýkora**
- **Greg Knaddison** Drupal 10 Security team member who can help us coordinate with them
- **Allison Vorthmann** from Herodevs, who are planning a Never Ending Drupal 7 support product

If you are interested in continuing security support for a Drupal 7 module please check our members page about how to join!
### Drupal 7 Message module support
---
The Drupal 7 version of the Message module got unsupported on drupal.org and D7Security group took it over. We forked it on Gitlab.com, made a 7.x-1.13 release and marked it as supported in the D7Security group.
If you would like to get update notifications about D7Security supported projects please follow the user guide.
### d7security.org website
---
Allison Vorthmann is looking into implementing a design for the d7security.org static website, to provide information and news. Thank you Allison!
If you would like to help with the website or any other topic feel free to reach out to us
