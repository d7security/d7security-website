---
slug: 05-19-2024-d7security-team-newsletter
title: The D7Security Newsletter May 19, 2024
authors:
  name: Klaus Purer
  title: Principal Engineer at Jobiqo
  image_url: ./img/profile-images/klausi.jpeg
tags: [Drupal7, D7Security, Newsletter]
date: 05-19-2024
---

Welcome to the fourth D7Security newsletter!

### Presentation at Drupal Austria meetup
---
I did a short presentation about D7Security at the [Drupal Austria March Meetup](https://www.meetup.com/drupal-austria/events/299119258/). I recorded the session afterwards and you can [watch it on YouTube](https://www.youtube.com/watch?v=1vLU1Eo0Tkk).
### Featured D7Security session at Drupal Dev Days Bulgaria
---
On June 26th I will talk about [D7Security again at Drupal Dev Days Burgas](https://ddd2024.drupalcamp.bg/drupal-dev-days-2024/session/d7security-drupal-7-long-term-support). I will try to outline options for Drupal 7 site owners from migration to long term support and I'm looking forward to a discussion what developers need to handle their Drupal 7 projects in the future. Let me know if you have any topic that could fit and is not in the talk description yet!
### Security advisory process established
---
The D7Security group now publishes security releases with accompanying security advisory posts on the website, very similar to what the Drupal Security Team does on drupal.org. Check out our [first advisory post for the Coffee module](/security-advisories/D7SECURITY-SA-CONTRIB-2024-001).
### Help wanted for the D7Security website
---
The work on the d7security.org website is ongoing, but the people working on it are busy and could use some help. We have a basic design and now need to fix links and dummy text. If you are interested to contribute please reach out to me or Allison!
### Commercial extended support for Drupal 7
---
I'm in contact with 2 commercial vendors that will offer extended support for Drupal 7 (HeroDevs and Tag1 Consulting). They could be interesting for organizations that need compliance security guarantees for their Drupal 7 projects or need other ongoing Drupal 7 coverage. My goal is to collaborate with those vendors in the D7Security group, so that they release their security fixes in the D7Security open source project. If we get a commitment from them to participate in the D7Security project then we can promote and recommend them for site owners seeking commercial contracts.
### Drupal 7 beyond January 2025
---
Roughly [300,000 sites are still running on Drupal 7](https://www.drupal.org/project/usage/drupal), down from 400k sites a year ago. The official Drupal 7 end-of-life date is 8 months away and I expect more sites moving away from Drupal 7 at a faster rate before that. I assume that more than 100k Drupal 7 sites will still be running after January 2025.
I'm working with clients that will likely still run on Drupal 7 beyond January. It is becoming even more clear to me that a Drupal 7 long term support solution is needed and that we need a central place to continue to maintain Drupal 7 core and selected contrib projects. D7Security could be the open source collaboration place to take over Drupal 7 maintenance. This will be interesting to plan in the next months and I hope to get Drupal 7 developers on board that need to do this work anyway.
That is all for today, please reach out in our [communication channels](/docs/communication) if you have any questions!
