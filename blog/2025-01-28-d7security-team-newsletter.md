---
slug: 01-28-2025-d7security-team-newsletter
title: The D7Security Newsletter Jan 28, 2025
authors:
  name: Sinduri Guntupalli
  title: Product Manager at drunomics
  image_url: ./img/profile-images/sinduri.jpg
tags: [Drupal7, D7Security, Newsletter]
date: 01-28-2025
---

Welcome to the sixth edition of the D7Security Newsletter!

### New D7Security members

We are excited to have new members join the D7Security group:

* Henri Médot ([anrikun on drupal.org](https://www.drupal.org/u/anrikun), [anrikun on gitlab.com](https://gitlab.com/anrikun))
* MustangGB ([mustanggb on drupal.org](https://www.drupal.org/u/mustanggb), [mustanggb on gitlab.com](https://gitlab.com/mustanggb))

### Thank you to the active members.

* Thanks to all the active members on D7Security. We are suporting 135 modules as of today. Check out the [Supported Drupal 7 projects.](https://gitlab.com/d7security/d7security/-/wikis/Supported-Drupal-7-projects#maintainers-for-projects)
* A big shout out to MustangGB (93 modules), Henri Médot (19 modules) and Vijay Mani (14 modules) for supporting so many modules.

### Marking D7 projects supported again we care about

* D7Security is focusing on security fixes and extended support for Drupal 7, but currently lacks core maintainers. We are seeking community volunteers to help maintain both core and contributed projects. A group is being formed to support Drupal 7 core, and we encourage members to join D7Security group and contribute. [Learn more in the issue and contact us if you would like to join us.](https://gitlab.com/d7security/d7security/-/issues/16)
* Instead of forking hundreds of repositories, we plan to only apply updates as necessary, with clear instructions for adding projects to the supported list and designating maintainers on our [wiki page](https://gitlab.com/d7security/d7security/-/wikis/Supported-Drupal-7-projects#maintainers-for-projects).

### Commercial Extended Support Provider Updates

* Currently the commercial providers are not publicly releasing their fixes. Tag1 [has announced in their FAQ](https://d7es.tag1.com/faq) that they will publish D7ES patches eventually.
* Tag1 released the coffee module security update, crediting us, the D7Security group. We are happy to collaborate with extended security support partners. [Read more in our security advisory page.](/security-advisories/D7SECURITY-SA-CONTRIB-2024-001)

### How you can help

Here are some opportunities how you can get involved and help us:
* [Install the d7security_client 7.x-1.3 according to our user guide](/docs/general-information/user-guide/) on your Drupal 7 site(s)
* Help us [set up an RSS feed on d7security.org](https://gitlab.com/d7security/d7security/-/issues/10) so that blog posts will be aggregated to [Drupal Planet](https://www.drupal.org/planet)

That is all for today, please reach out in our [communication channels](/docs/communication) if you have any questions!
