---
slug: 10-09-2024-d7security-team-newsletter
title: The D7Security Newsletter Oct 9, 2024
authors:
  name: Klaus Purer
  title: Principal Engineer at Jobiqo
  image_url: ./img/profile-images/klausi.jpeg
tags: [Drupal7, D7Security, Newsletter]
date: 10-09-2024
---

Welcome to the fifth D7Security newsletter!

### d7security.org website launch

Our website has launched at https://www.d7security.org/ 🚀

This is a statically generated website built with Docusaurus in our [Gitlab Pages repository](https://gitlab.com/d7security/d7security-website). Thank you to our D7Security members Sinduri Guntupalli and Allison Vorthman for building and launching it!

### Blog post about unsupported modules

Klausi wrote a blog post about Drupal 7 modules losing security support before Drupal 7 core: [What happens when a Drupal 7 module used by 70,000 sites gets unsupported?](/blog/08-23-2024-what-happens-when-a-drupal-7-module-used-by-70000-sites-gets-unsupported/)

### Telemetry to collect D7Security usage data

On drupal.org [project usage data is collected](https://www.drupal.org/project/usage/drupal) to get an idea how many sites use Drupal 7 core or contributed projects. We want to do the same and implemented a [small data collection server](https://gitlab.com/d7security/d7security_telemetry) for D7Security. It stores only aggregated usage statistics (which projects are installed in which version) to preserve privacy of Drupal 7 sites. This will give us insights into which projects are most used.

Please help us collect statistics by [installing d7security_client 7.x-1.3 according to our user guide](/docs/general-information/user-guide/)!

### New D7Security supported modules

A couple of Drupal 7 modules have gone unsupported on drupal.org and we adopted them in the D7Security group:

* [autocomplete_deluxe](https://gitlab.com/d7security/autocomplete_deluxe)
* [maintenance200](https://gitlab.com/d7security/maintenance200)
* [views_slideshow](https://gitlab.com/d7security/views_slideshow)

### New D7Security members

I'm happy to report that we added new members to the D7Security group:

* Yogesh Pawar ([yogeshmpawar on drupal.org](https://www.drupal.org/u/yogeshmpawar), [yogeshmpawar on gitlab.com](https://gitlab.com/yogeshmpawar))
* Ayush Mishra ([ayushmishra206 on drupal.org](https://www.drupal.org/u/ayushmishra206), [ayushmishra206 on gitlab.com](https://gitlab.com/ayushmishra206))
* Sinduri Guntupalli ([sinduri on drupal.org](https://www.drupal.org/u/sinduri), [sinduri.g on gitlab.com](https://gitlab.com/sinduri.g))
* Alex Burrows ([aburrows on drupal.org](https://www.drupal.org/u/aburrows), [abdesignuk on gitlab.com](https://gitlab.com/abdesignuk))
* Paul McKibben ([paulmckibben on drupal.org](https://www.drupal.org/u/paulmckibben), [paulmckibben on gitlab.com](https://gitlab.com/paulmckibben))
* Ra Mänd ([ram4nd on drupal.org](https://www.drupal.org/u/ram4nd), [ram4nd on gitlab.com](https://gitlab.com/ram4nd))
* Alex Pott ([alexpott on drupal.org](https://www.drupal.org/u/alexpott), [alexpott on gitlab.com](https://gitlab.com/alexpott))
* Vijay Mani ([vijaycs85 on drupal.org](https://www.drupal.org/u/vijaycs85), [vijaycs85 on gitlab.com](https://gitlab.com/vijaycs85))

### Commercial extended support for Drupal 7

There are now 3 official [commercial providers for Drupal 7 extended support listed on drupal.org](https://www.drupal.org/about/drupal-7/d7eol/migration-resource-center/d7-extended-security-support). It is still unclear how they will cooperate to fix and release Drupal 7 security updates.

### How you can help

Here are some opportunities how you can get involved and help us:
* [Install the d7security_client 7.x-1.3 according to our user guide](/docs/general-information/user-guide/) on your Drupal 7 site(s)
* Help us [set up an RSS feed on d7security.org](https://gitlab.com/d7security/d7security/-/issues/10) so that blog posts will be aggregated to [Drupal Planet](https://www.drupal.org/planet)
* Work with the Drupal 7 soft landing initiative to mention D7Security in the [documentation on drupal.org](https://www.drupal.org/community-initiatives/drupal-7-soft-landing-initiative) ([Discussion issue](https://gitlab.com/d7security/d7security/-/issues/9))


That is all for today, please reach out in our [communication channels](/docs/communication) if you have any questions!
